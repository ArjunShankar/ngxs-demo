import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Todo } from '../../models/todo.model';
import { TodoState } from '../../state/todo.state';
import { Observable } from 'rxjs/Observable';
import { RemoveItem } from '../../actions/todo.actions';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html'
})
export class ReadComponent implements OnInit {

  //dataProvider: Observable<Todo>;

  @Select(TodoState.getTodos) dataProvider: Observable<Todo>;  // either use this ... or subscribe to the state ... as shown in constructor.

  constructor(
    private store: Store
  ) {
    //this.dataProvider = this.store.select(state => state.todos.todos);
  }

  delete(input) {
    this.store.dispatch(new RemoveItem(input));
  }

  ngOnInit() {
  }

}
