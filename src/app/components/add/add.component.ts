import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AddItem } from '../../actions/todo.actions';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  add(name, data) {
    this.store.dispatch(new AddItem({name: name, details: data}));
  }

}
