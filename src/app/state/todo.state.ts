import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Todo } from './../models/todo.model';
import { AddItem, RemoveItem } from './../actions/todo.actions';


export class TodoStateModel {
  todos: Todo[];
}

@State<TodoStateModel>({
  name: 'todos',
  defaults: {
    todos: []
  }
})

export class TodoState {

  @Selector()
  static getTodos(state: TodoStateModel) {
    return state.todos;
  }

  @Action(AddItem)
  add({getState, patchState }: StateContext<TodoStateModel>, { payload }: AddItem) {
    const state = getState();
    patchState({
      todos: [...state.todos, payload]
    });
  }

  @Action(RemoveItem)
  remove({getState, patchState }: StateContext<TodoStateModel>, { payload }: RemoveItem) {
    patchState({
      todos: getState().todos.filter(a => a.name !== payload)
    });
  }

}
