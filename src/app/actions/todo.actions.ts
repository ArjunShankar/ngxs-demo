import { Todo } from './../models/todo.model'

export class AddItem {
  static readonly type = '[TODO] Add'

  constructor(public payload: Todo) {}
}

export class RemoveItem {
  static readonly type = '[TODO] Remove'

  constructor(public payload: string) {}
}
